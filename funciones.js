/**
 * autor Jorge Jaramillo
 * escrito el dia 19 de octubre de 2021
 */


const readline = require('readline')
const convertValue = new Intl.NumberFormat();
const preguntas=['Nombre del Docente: ','Asignatura: ','Horas Trabajadas: ']
const v_hora = 31000;
const semanas = 4;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const askAndAnswer = (pregunta)=>{
    return new Promise((resolve)=>{
        rl.question(pregunta,(value)=>{
            resolve(value)
        })    
    })
}

const result = (datos) =>{
    console.log(`
    --------RESULTADO-------
    Docente: ${datos["0"]}
    Asignatura: ${datos["1"]}
    Horas Semanales: ${datos["2"]}
    Total Horas:  ${convertValue.format(datos["2"]*semanas)}
    Salario Mensual:  ${convertValue.format((datos["2"]*v_hora)*semanas)}
    `)
    rl.close()
}

const inicioApp = async () => {
    const respuestas = {}
        console.log('\n-------------Corporación Universidad mis primeros Sueños-------------')
    for (let [indice, pregunta] of preguntas.entries()) {
        const resultado = await askAndAnswer(pregunta)
        respuestas[indice] = resultado
    }

    while (19 < respuestas["2"] || !Number(respuestas["2"])) {
    respuestas["2"] = await askAndAnswer(
        "Error, No puede ingresar mas de 19 horas o esta mal escrito: "
    )
    }

    result(respuestas)
}

module.exports.inicioApp = inicioApp

